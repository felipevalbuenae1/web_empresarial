from django.shortcuts import render

# Create your views here.
def Index(request):
    name="Index"
    return render(request,"core/index.html",{"name":name})

def About(request):
    name="About"
    return render(request,"core/about.html",{"name":name})

def Contact(request):
    name="Contact"
    return render(request,"core/contact.html",{"name":name})

def Sample(request):
    name="Sample"
    return render(request,"core/sample.html",{"name":name})


def Store(request):
    name="Store"
    return render(request,"core/store.html",{"name":name})