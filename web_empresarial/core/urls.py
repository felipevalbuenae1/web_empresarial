
from django.urls import path
from core import views

urlpatterns =[
    path("",views.Index,name="Index"),
    path("about/",views.About,name="About"),
    path("contact/",views.Contact,name="Contact"),
    path("sample/",views.Sample,name="Sample"),
    path("store/",views.Store,name="Store"),
]