from django.shortcuts import render
from .models import Service

# Create your views here.
def Services(request):
    services = Service.objects.all()
    name = "Services"
    return render(request,"services/services.html",{"name":name,"services":services})