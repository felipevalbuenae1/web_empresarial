from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=200,verbose_name = "nombre")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Media:
        Verbose_name="categoria"
        ordering = ["-created"]
    def __str__(self):
        return self.name

class Post(models.Model):
    title = models.CharField(max_length=200,verbose_name = "nombre")
    content =  models.TextField(verbose_name = "contenido")
    image = models.ImageField(verbose_name="imagen", null=True, blank=True)
    published = models.DateTimeField(default=now)
    author = models.ForeignKey(User,on_delete = models.CASCADE)
    categories = models.ManyToManyField(Category,related_name="get_posts")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Media:
        verbose_name="entrada"
        verbose_name_plural = "entradas"
        ordering = ["-created"]
    def __str__(self):
        return self.title